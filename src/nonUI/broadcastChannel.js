// name: String, data: object | DOMString
function sendMessage (name = '', data = undefined) {
  if (window === window.parent)
    return
  // allow any targetOrigin; no sensitive data is broadcasted anyway
  window.parent.postMessage({ action: name, data, timestamp: Date.now() }, '*')
}

// debugMode: Boolean, graphListCallback: (graphList) => void, graphDataCallback: (graphData) => void
function listenForMessages (debugMode = false, graphListCallback, graphDataCallback) {
  window.addEventListener('message', (event) => {
    if (debugMode)
      console.log('Received event', event.data)
    if (event.data.kind === 'graphList')
      graphListCallback(event.data.data)
    else if (event.data.kind === 'graphData')
      graphDataCallback(event.data.data)
    else if (debugMode)
      console.warn('Received unexpected message. Discarding it', event)
  }, false)
}

export { sendMessage, listenForMessages }
