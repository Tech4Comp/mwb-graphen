import isEmpty from 'lodash/isEmpty'
import isArray from 'lodash/isArray'
import { Set } from 'immutable' // List, Set, Record
import parser from 'fast-xml-parser'
import he from 'he'
import chroma from 'chroma-js'

const options = {
  attributeNamePrefix: '',
  attrNodeName: false,
  textNodeName: '#id',
  ignoreAttributes: false,
  ignoreNameSpace: false,
  allowBooleanAttributes: false,
  parseNodeValue: true,
  parseAttributeValue: false,
  trimValues: true,
  cdataTagName: '__cdata',
  cdataPositionChar: '\\c',
  parseTrueNumberOnly: false,
  arrayMode: false,
  attrValueProcessor: (val, attrName) => he.decode(val, { isAttributeValue: true }),
  tagValueProcessor: (val, tagName) => he.decode(val),
  stopNodes: ['parse-me-as-string']
}

// file = html input file result
function isGraphMLFile (file) {
  if (file.name.endsWith('.graphml') && (file.type === 'text/xml' || file.type === ''))
    return true
  else
    return false
}
// data = XML as String
// TODO maybe convert to https://github.com/graphology/graphology-graphml#readme
function convertFile (data, respectGroups = true, nodeColor = undefined) {
  if (parser.validate(data) === true) {
    const jsonObj = parser.parse(data, options)
    if (!isEmpty(jsonObj.graphml) && !isEmpty(jsonObj.graphml.graph) && !isEmpty(jsonObj.graphml.graph.edge) && !isEmpty(jsonObj.graphml.graph.node)) {
      // console.log(jsonObj)
      const nodeKeys = {
        color: getKey(jsonObj, 'node', 'color'),
        defaultColor: getKey(jsonObj, 'node', 'color', 'default'),
        area: getKey(jsonObj, 'node', 'area')
      }
      const edgeKeys = {
        color: getKey(jsonObj, 'edge', 'color'),
        defaultColor: getKey(jsonObj, 'edge', 'color', 'default'),
        area: getKey(jsonObj, 'edge', 'area'),
        weight: getKey(jsonObj, 'edge', 'weight')
      }
      const nodes = Set().asMutable()
      const edges = Set().asMutable()
      jsonObj.graphml.graph.node.forEach(node => {
        let color = isArray(node.data) ? node.data.find(el => el.key === nodeKeys.color)['#id'] : node.data['#id']
        let group = isArray(node.data) ? node.data.find(el => el.key === nodeKeys.area)['#id'] : undefined
        if (color === undefined) color = nodeKeys.defaultColor
        if (color === '#C0C040') color = '#F1C40F' // fix ugly T-Mitocar default color
        if (nodeColor !== undefined && color === '#F1C40F') color = nodeColor
        group = (group === undefined) ? 'undefined' : group
        const targetNode = { id: node.id, label: node.id, group, color }
        if (!respectGroups)
          delete targetNode.group
        nodes.add(targetNode)
      })
      const colorScaleDomain = calculateEdgeColorScale(jsonObj.graphml.graph.edge, edgeKeys)
      const edgeColorScale = chroma.bezier(['0D47A1', 'D32F2F']).scale().domain(colorScaleDomain).nodata('0D47A1')
      jsonObj.graphml.graph.edge.forEach(edge => {
        let color = (edge.data.find(el => el.key === edgeKeys.color) || {})['#id']
        let group = (edge.data.find(el => el.key === edgeKeys.area) || {})['#id']
        const weight = (edge.data.find(el => el.key === edgeKeys.weight) || {})['#id']
        if (color === undefined) color = edgeKeys.defaultColor
        if (weight !== undefined) color = edgeColorScale(weight).hex() // fix ugly T-Mitocar default color
        group = (group === undefined) ? 'undefined' : group
        const targetEdge = { source: edge.source, target: edge.target, color, group }
        if (weight !== undefined)
          targetEdge.weight = weight
        if (!respectGroups)
          delete targetEdge.group
        edges.add(targetEdge)
      })

      return { nodes: nodes.toJS(), edges: edges.toJS() }
    }
  } else {
    // "fixes" that some corejs module isn't found at build time when throw new Error() is used.
    throw 'XML file not valid' // eslint-disable-line
  }
}

function calculateEdgeColorScale (edges, edgeKeys) {
  let weightScale = [1, 0]
  edges.forEach(edge => {
    const weight = (edge.data.find(el => el.key === edgeKeys.weight) || {})['#id']
    if (weight !== undefined) {
      if (weight < weightScale[0])
        weightScale[0] = weight
      else if (weight > weightScale[1])
        weightScale[1] = weight
    }
  })
  if (weightScale[0] === 1 && weightScale[1] === 0)
    weightScale = [0, 1]
  return weightScale
}

function getKey (jsonObj, type, name, el = 'id') {
  return (jsonObj.graphml.key.find(el => el.for === type && el['attr.name'] === name) || {})[el]
}

export { isGraphMLFile, convertFile }
