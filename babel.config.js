module.exports = {
  presets: [
    ['@babel/preset-env'],
    ['@vue/babel-preset-app'],
    ['@babel/preset-react']
  ],
  plugins: [
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    ['@babel/plugin-proposal-private-property-in-object', { loose: true }],
    ['@babel/plugin-proposal-private-methods', { loose: true }],
    ['@babel/plugin-syntax-jsx'],
    ['@babel/plugin-transform-react-jsx'],
    ['@babel/plugin-syntax-dynamic-import'],
    ['@babel/plugin-transform-runtime'],
    ['wildcard', {
      exts: ['graphml'],
      noModifyCase: true
    }]

  ]
}
