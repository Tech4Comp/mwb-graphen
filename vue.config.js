module.exports = {
  runtimeCompiler: true,
  configureWebpack: {
    resolve: {
      fallback: {
        // fs: false,
        // url: require.resolve('url/'),
        // util: require.resolve('util/'),
        // assert: require.resolve('assert/'),
        // http: require.resolve('stream-http'),
        // zlib: require.resolve('browserify-zlib'),
        // path: require.resolve('path-browserify'),
        // https: require.resolve('https-browserify'),
        stream: require.resolve('stream-browserify')
        // crypto: require.resolve('crypto-browserify')
        // querystring: require.resolve('querystring-es3')
      }
    },
    module: {
      rules: [{
        test: /\.graphml$/,
        loader: 'raw-loader'
      }]
    }
  }
}
